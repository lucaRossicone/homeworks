package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Scanner;

import org.apache.lucene.codecs.simpletext.SimpleTextCodec;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import utility.Merger;
import utility.Parser;
import utility.Stats;

public class Main {

    public static void main(String[] args) throws IOException {

        /**
         * dichiarazione variabili
         */
        long timeA = System.currentTimeMillis();
        Path path = Paths.get("target/idx");
        Directory directory = FSDirectory.open(path);
        IndexWriterConfig config = new IndexWriterConfig();
        config.setCodec(new SimpleTextCodec());

        IndexWriter writer = new IndexWriter(directory, config);
        // writer.deleteAll();

        /**
         * lettura dal file di config
         */
        FileReader f = new FileReader("config.txt");
        BufferedReader b = new BufferedReader(f);

        LinkedList<String> ls = new LinkedList<String>();
        String s = b.readLine();
        do {
            ls.add(s);
            s = b.readLine();
        } while (s != null);

        f.close();
        b.close();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to execute parsing? [Y/n]");
        char c = scanner.nextLine().charAt(0);
        do {
            if (c == 'y' || c == 'Y') {
                System.out.println("Beginning parsing");
                Parser parser = new Parser(ls, writer);
                parser.parse();
                scanner.close();
            } else if (c == 'n' || c == 'N')
                break;
            else {
                System.out.println("Unknown answer. Try again");
                System.out.println("Do you want to execute parsing? [Y/n]");
                c = scanner.nextLine().charAt(0);
            }
        } while (c != 'y' && c != 'n' && c != 'Y' && c != 'N');

        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Parsing terminated. Do you want to elaborate stats? [Y/n]");
        char c2 = scanner2.nextLine().charAt(0);
        do {
            if (c2 == 'y' || c2 == 'Y') {
                scanner2.close();
                Stats stats = new Stats(ls.getFirst());
                stats.calculateStats();
            } else if (c2 == 'n' || c2 == 'N')
                break;
            else {
                System.out.println("Unknown answer. Try again");
                System.out.println("Parsing terminated. Do you want to elaborate stats? [Y/n]");
                c2 = scanner2.nextLine().charAt(0);
            }
        } while (c2 != 'y' && c2 != 'n' && c2 != 'Y' && c2 != 'N');

        /**
         * chiusura del buffer
         */
        writer.commit();
        writer.close();

        if (c == 'n' || c == 'N')
            ls.poll();
        Merger merger = new Merger(directory);
        merger.merge(ls);

        directory.close();

        long timeB = System.currentTimeMillis();
        System.out.println("\nEnd of elaboration. Elapsed time: " + (timeB - timeA) + "ms");
    }
}