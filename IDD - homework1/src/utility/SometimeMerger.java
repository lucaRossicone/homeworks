package utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.Map.*;

import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Scanner;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SometimeMerger {

    private IndexReader reader;
    private IndexSearcher searcher;
    private Map<String, Integer> set2count;

    public SometimeMerger(Directory directory, String inputFile) throws IOException {
        this.reader = DirectoryReader.open(directory);
        this.searcher = new IndexSearcher(reader);
        this.set2count = new HashMap<String, Integer>();
    }

    public void merge(LinkedList<String> ls) throws IOException {

        System.out.println("\nExecuting merging...\n");

        int size = 0;

        try {
            FileInputStream f = new FileInputStream("files/parsedSize.luc");
            size = f.read();
            f.close();
        } catch (Exception e) {
            System.out.println(
                    "Attento, non esiste nessun file che contenga la dimensione. Forse non hai effettuato la fase di parsing,"
                            + "oppure vuoi inserire una dimensione a mano?");
            Scanner scanner = new Scanner(System.in);
            size = scanner.nextInt();
            scanner.close();
        }

        System.out.println("Searching " + ls.size() + " terms.\n");

        for (String s : ls) {

            System.out.println("I'm searching term " + "\"" + s + "\".\n");

            Query query = new TermQuery(new Term("contenuto", s));
            TopDocs hits = searcher.search(query, size);
            for (int i = 0; i < hits.scoreDocs.length; i++) {
                ScoreDoc scoreDoc = hits.scoreDocs[i];
                Document doc = searcher.doc(scoreDoc.doc);
                String index = String.valueOf(doc.get("index"));

                if (this.set2count.containsKey(index)) {
                    this.set2count.replace(index, this.set2count.get(index) + 1);
                    System.out.println("I found again term " + s);
                } else
                    set2count.put(index, 1);
            }
        }

        List<Entry<String, Integer>> list = new LinkedList<>(this.set2count.entrySet());

        Collections.sort(list, new Comparator<Object>() {

            @SuppressWarnings("unchecked")
            public int compare(Object o2, Object o1) {
                return ((Comparable<Integer>) ((Map.Entry<String, Integer>) (o1)).getValue())
                        .compareTo(((Map.Entry<String, Integer>) (o2)).getValue());
            }
        });
        int i = 0;

        System.out.println("\nResults of merge list algorithm:");
        // Map<String, Integer> result = new LinkedHashMap<>();
        for (Iterator<Entry<String, Integer>> it = list.iterator(); it.hasNext() && i < 10; i++) {
            Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();
            System.out.println("doc" + entry.getKey() + " = " + entry.getValue());
        }
    }

    private static LinkedList<String> collectNumberOfDistinctValuesForColumn() throws IOException {

        FileReader f = new FileReader("/run/media/soros/Nembus/homeworks/ignore/tables.json");
        BufferedReader b = new BufferedReader(f);
        String s = new String();
        LinkedList<String> lista = null;
        int i;
        System.out.println("Now collecting number of distinct values for column stats. Please wait...");
        for (i = 0;; i++) {
            s = b.readLine();
            if (s == null)
                break;

            if (i == 300000)
                System.out.println("We are halfway there...");

            JsonElement jsonTree = JsonParser.parseString(s);
            JsonObject table = jsonTree.getAsJsonObject();

            JsonArray cells = table.getAsJsonArray("cells");

            int colonne = table.getAsJsonObject("maxDimensions").get("column").getAsInt();

            HashMap<Integer, HashSet<String>> contaColonne = new HashMap<Integer, HashSet<String>>();
            for (int j = 0; j <= colonne; j++) {
                contaColonne.put(j, new HashSet<String>());
            }

            int length = cells.size();
            for (int j = 0; j < length; j++) {
                JsonObject cell = cells.get(j).getAsJsonObject();
                int colonna = cell.getAsJsonObject("Coordinates").get("column").getAsInt();
                if (!cell.get("isHeader").getAsBoolean() && !cell.get("cleanedText").getAsString().equals("")) {
                    contaColonne.get(colonna).add(cell.get("cleanedText").getAsString());
                }
            }
            if (i == 4) {
                lista = giveMeColumns(contaColonne.get(0));
                break;
            }
        }
        b.close();
        f.close();
        return lista;
    }

    private static LinkedList<String> giveMeColumns(HashSet<String> set) {
        LinkedList<String> list = new LinkedList<String>();

        set.forEach(v -> list.add(v));
        return list;
    }

    public static void main(String[] args) throws IOException {

        Path path = Paths.get("target/idx");
        Directory directory = FSDirectory.open(path);
        // IndexWriterConfig config = new IndexWriterConfig();
        // config.setCodec(new SimpleTextCodec());

        // IndexWriter writer = new IndexWriter(directory, config);

        /**
         * lettura dal file di config
         */
        FileReader f = new FileReader("config.txt");
        BufferedReader b = new BufferedReader(f);

        LinkedList<String> ls = new LinkedList<String>();
        String s = b.readLine();
        do {
            ls.add(s);
            s = b.readLine();
        } while (s != null);

        f.close();
        b.close();

        LinkedList<String> list = new LinkedList<String>();
        list = collectNumberOfDistinctValuesForColumn();

        long timeA = System.currentTimeMillis();

        SometimeMerger merger = new SometimeMerger(directory, ls.getFirst());
        merger.merge(list);

        directory.close();

        long timeB = System.currentTimeMillis();
        System.out.println("\nEnd of elaboration. Elapsed time: " + (timeB - timeA) + "ms");
    }
}
