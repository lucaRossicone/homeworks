package utility;

import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.Styler.*;
import org.knowm.xchart.*;

/**
 * Charts
 */
public class Charts {

    public static void main(String[] args) throws IOException {

        // Create Chart
        final CategoryChart chart = new CategoryChartBuilder().width(1920).height(1080).title("Distribuzione del numero di valori distinti per colonna").
        xAxisTitle("# Valori Distinti").yAxisTitle("# Colonne").build();

        final XYChart chart2 = new XYChartBuilder().width(1920).height(1080).title("Performance").xAxisTitle("# Elementi della query").yAxisTitle("Tempo di esecuzione (in ms)").build();

        // Customize Chart
        chart.getStyler().setLegendVisible(false);
        chart.getStyler().setAxisTicksVisible(false).setYAxisMin((double) 1);//setAxisTicksLineVisible(false).setAxisTicksMarksVisible(false).;
        // chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Area);

        chart2.getStyler().setLegendVisible(false);


        // Stats s = new Stats();
        // s.collectDifferent();
        // int[] a1 = s.getA1();
        // int[] a2 = s.getA2();
        // Series
        // chart.addSeries("a", a1, a2);
        chart2.addSeries("a", new double[] {2,5,11,20,30,38}, new double[] {205820, 201312, 212228,324898,353031,434931});
        // chart.addSeries("a", new double[] { 0, 3, 5, 7, 9 }, new double[] { -3, 5, 9, 6, 5 });
        // chart.addSeries("b", new double[] { 0, 2, 4, 6, 9 }, new double[] { -1, 6, 4, 0, 4 });
        // chart.addSeries("c", new double[] { 0, 1, 3, 8, 9 }, new double[] { -2, -1, 1, 0, 1 });

        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                // Create and set up the window.
                JFrame frame = new JFrame("Advanced Example");
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                // chart
                // JPanel chartPanel = new XChartPanel<CategoryChart>(chart);
                JPanel chartPanel = new XChartPanel<XYChart>(chart2);
                frame.add(chartPanel, BorderLayout.CENTER);

                // label
                JLabel label = new JLabel("Performance", SwingConstants.CENTER);
                frame.add(label, BorderLayout.SOUTH);

                // Display the window.
                frame.pack();
                frame.setVisible(true);
            }
        });
    }

}