package utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Join {

    private String inputFile;
    private int index;

    public Join(String inputFile, int index) throws IOException {
        this.inputFile = inputFile;
        this.index = index;
    }

    private void collectEntries() throws IOException {

        FileReader f = new FileReader(this.inputFile);
        BufferedReader b = new BufferedReader(f);
        String s = new String();

        int i, righe, colonne;
        String[][] matrix;
        System.out.println("Collecting table entries...");
        for (i = 0;; i++) {

            s = b.readLine();
            if (i == index){
                JsonElement jsonTree = JsonParser.parseString(s);
                JsonObject table = jsonTree.getAsJsonObject();
                JsonObject dimensioni = table.getAsJsonObject("maxDimensions");
                righe = dimensioni.get("row").getAsInt()+1;
                colonne = dimensioni.get("column").getAsInt()+1;
                matrix = new String[righe][colonne];

                JsonArray cells = table.getAsJsonArray("cells");
                for (int j = 0; j < cells.size(); j++) {
                    JsonObject cell = cells.get(j).getAsJsonObject();
                    int colonna = cell.getAsJsonObject("Coordinates").get("column").getAsInt();
                    int riga = cell.getAsJsonObject("Coordinates").get("row").getAsInt();
                    s = cells.get(j).getAsJsonObject().get("cleanedText").getAsString();
                    matrix[riga][colonna] = s;
                }
                break;
            }

                
        }
        for (int k=0; k<righe; k++)
            {
                for (int q=0; q<colonne; q++)
                    System.out.print(matrix[k][q]+" | ");
                System.out.println("");
            }
        b.close();
        f.close();
    }

    

    public static void main(String[] args) throws IOException {

        Join join = new Join("/run/media/soros/Nembus/homeworks/ignore/tables.json", 191056);
        join.collectEntries();
    }
}
