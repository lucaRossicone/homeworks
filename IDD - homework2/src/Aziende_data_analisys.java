import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.opencsv.CSVReader;

public class Aziende_data_analisys {

    public static void trovaDoppi(Reader reader, Reader reader_) throws Exception {

            List<String[]> list = new ArrayList<>();
            CSVReader csvReader = new CSVReader(reader);
            String[] line;
            csvReader.readNext();
            while ((line = csvReader.readNext()) != null) {
                String name = line[0].toLowerCase();
                line = name.split(" ");
                list.add(line);
            }
            reader.close();
            csvReader.close();

            System.out.println("Creata la prima lista...");

            String[] stopwords = {"&","company", "corporation", "holding", "holdings", "international",
                "us", "oyj", "ag", "co.", "national", "group", "(publ)", "ab", "", "industries", "spa",
                "ica", "bank", "of", "oil", "co", "america", "china", "the", "systems", "and", "a/s", "services",
                "(tcs)", "construction", "limited", "se", "united", "royal", "state", "general", "de", "managment"
            };
            int count = 0;
            HashSet<String> set = new HashSet<>();
            for (String word : stopwords) {
                set.add(word);
            }
            String[] tmp = {""};
            CSVReader csvReader_ = new CSVReader(reader_);
            csvReader_.readNext();
            while ((line = csvReader_.readNext()) != null) {
                String name = line[0].toLowerCase();
                line = name.split(" ");
                for (String string : line ) {
                    if (!set.contains(string)){
                        set.add(string);
                        for (String[] array : list) {
                            for (String azienda : array) {
                                if (string.equals(azienda)){
                                    if (!tmp[0].equals(array[0])) {
                                        count++;
                                        System.out.print(count + ": ");
                                        for (String s : array) System.out.print(s + " ");
                                        System.out.print("|| ");
                                        for (String s : line) System.out.print(s + " ");
                                        System.out.println();
                                    }
                                    tmp = array;
                                } 
                            }
                        }
                    }
                }
            }
            reader_.close();
            csvReader_.close();
            System.out.println(count);
    }

    public static void trovaRipetuti(Reader reader) throws Exception {

        CSVReader csvReader = new CSVReader(reader);
        String[] line;
        HashMap<String, Integer> map = new HashMap<>();
        csvReader.readNext();
        while ((line = csvReader.readNext()) != null) {
            String name = line[0];
            if (map.containsKey(name))
                map.put(name, map.get(name) + 1);
            else
                map.put(name, 1);
        }
        reader.close();
        csvReader.close();

        HashMap<Integer, Integer> map_ = new HashMap<>();
        map.forEach((k, v) -> {
            if (map_.containsKey(v))
                map_.put(v, map_.get(v) + 1);
            else
                map_.put(v, 1);
        });

        System.out.println("\n===== Distribuzione ripetizioni =====");
        map_.forEach((k, v) -> System.out.println("Un numero di entità pari a " + v + " è ripetuto esattamente " + k + " volte."));
        System.out.println("\n===== Numero entità distinte =====");
        System.out.println(map.size());

    }

    public static void trovaNulli(Reader reader) throws Exception {
        CSVReader csvReader = new CSVReader(reader);
        String[] line;
        String[] intestazione = csvReader.readNext();
        int[] array = new int[intestazione.length];
        for (int i = 0; i < array.length; i++)
            array[i] = 0;

        while ((line = csvReader.readNext()) != null) {
            for (int i = 0; i < array.length; i++) 
                if (line[i].equals("")) array[i]++;
        }
        reader.close();
        csvReader.close();

        System.out.println("\n===== Numero di valori nulli per colonna =====");
        for (int i = 0; i < array.length; i++) {
            System.out.println("La colonna " + intestazione[i] + " ha un numero di valori nulli pari a " + array[i] + ".");
        }
    }


    public static void main(String[] args) throws Exception {

        //trovaNulli(new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/aziendefilippo.csv"));
        //trovaRipetuti(new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/dottori.csv"));
        trovaDoppi(new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/aziende.csv"),new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/aziendefilippo.csv"));
    }


}


