package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.opencsv.CSVWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DottoriFilippo {

    public static String[] mettilaNormaleDaiNonSeSaMai(String str) {
        String ruolo = new String();
        String numero = new String();
        StringBuilder nome = new StringBuilder();

        List<String> items = Arrays.asList(str.split("\\s* \\s*"));

        Collections.reverse(items);
        for (int i = 0; i < items.size(); i++) {
            ruolo = items.get(1);
            numero = items.get(0);
            if (i > 1)
                nome.append(items.get(i) + " ");
        }
        String[] res = {nome.toString(), ruolo, numero};
        return res; 
    }

    public static void main(String[] args) throws IOException {

        List<String[]> finale = new ArrayList<String[]>();
        String[] intestazione = {"Name","Specialties","Affiliations","Years In Practice","Provider Gender","Language","Age Groups Seen", "Education", "Rating"};
        finale.add(intestazione);
        CSVWriter writer = new CSVWriter(new FileWriter("/home/zoso/Documents/IDD/homeworks/IDD - homework2/dottorifilippo1.csv"));
        
        for(int i=1; i<127; i++){
            Document doc = Jsoup.connect("https://doctors.beaumont.org/search?sort=networks%2Crelevance&unified=primary%20care&page="+String.valueOf(i)).get();
            Elements doctors = doc.select("h2 > a.css-likv8a-ProviderLink");
            int count = 0;
            for (Element doctorURL : doctors) {
                String doctor = doctorURL.attr("abs:href");
                //System.out.println(doctor);
                doc = Jsoup.connect(doctor).get();
                String name="", specialties="", affiliations="", years="", gender="", language="", age="", education="", rating="";
                    
                try {
                    name = doc.select("h2#provider-name").first().text().split(",")[0];
                    //System.out.println(name);
                } catch (NullPointerException e) {}
                try {
                    specialties = doc.select("div:matches(Specialties) + div > div > div").first().text();
                    //System.out.println(specialties);
                } catch (NullPointerException e) {}
                try {
                    affiliations = doc.select("div:matches(Affiliations) + div > div > div").first().text();
                } catch (NullPointerException e) {}
                try {
                    years = doc.select("div:matches(Years In Practice) + div > div > div").first().text();
                } catch (NullPointerException e) {}
                try {
                    gender= doc.select("div:matches(Provider Gender) + div > div > div").first().text();
                } catch (NullPointerException e) {}
                try {
                    language = doc.select("div:matches(Language) + div > div > div[class=pb-s about-panel-item col-xs-12 fc-gray-1]").text();
                    //System.out.println(language);
                } catch (NullPointerException e) {}
                try {
                    age = doc.select("div:matches(Age Groups Seen) + div > div > div[class=pb-s about-panel-item col-xs-12 fc-gray-1]").text();
                } catch (NullPointerException e) {}
                try {
                    rating = doc.select("span.summary-avg-rating").first().text();
                } catch (NullPointerException e) {}
                try {
                    education = doc.select("h2:matches(Education) + div > div > div > h3").first().text();
                } catch (NullPointerException e) {}
                
                
                count++;
                //System.out.println(count);
                String line[] = {name, specialties, affiliations, years, gender, language, age, education, rating};
                if (count % 100 == 0) System.out.println(count);
                //for (String s:line) System.out.println(s);
                finale.add(line);
            }
        }
       
        
        writer.writeAll(finale);
        writer.flush();

    }


        
}
