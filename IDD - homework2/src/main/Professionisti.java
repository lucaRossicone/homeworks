package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.opencsv.CSVWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Professionisti {

    public static String[] mettilaNormaleDaiNonSeSaMai(String str) {
        String ruolo = new String();
        String numero = new String();
        StringBuilder nome = new StringBuilder();

        List<String> items = Arrays.asList(str.split("\\s* \\s*"));

        Collections.reverse(items);
        for (int i = 0; i < items.size(); i++) {
            ruolo = items.get(1);
            numero = items.get(0);
            if (i > 1)
                nome.append(items.get(i) + " ");
        }
        String[] res = { nome.toString(), ruolo, numero };
        return res;
    }

    public static void main(String[] args) throws IOException {

        List<String[]> finale = new ArrayList<String[]>();
        String[] intestazione = { "Name", "Role", "#", "Height", "Weight", "Team", "Born", "Bplace", "Nationality",
                "Agent", "Drafted", "Hschool" };
        finale.add(intestazione);
        CSVWriter writer = new CSVWriter(
                new FileWriter("/home/soros/appunti/ingegneriaDeiDati/homeworks/IDD - homework2/basket.csv"));

        Document doc = Jsoup.connect("https://basketball.realgm.com/nba/players/2022?").get();
        Elements elements = doc.select("td[data-th=Player]");
        int count = 0;
        for (Element link : elements) {
            String linkHref = link.select("a").first().attr("abs:href");
            // System.out.println(linkHref);
            doc = Jsoup.connect(linkHref).get();

            String height = "", weight = "", team = "", born = "", bplace = "", nationality = "", agent = "",
                    drafted = "", hschool = "";
            String[] nrm = mettilaNormaleDaiNonSeSaMai(doc.select("h2[style=margin-top: 0;]").first().text());

            try {
                String split[] = doc.select("p:contains(Weight:)").first().text().split(" ");
                height = split[1];
                weight = split[4];
            } catch (NullPointerException e) {
            }
            try {
                team = doc.select("strong:contains(Current Team:) + a").first().text();
            } catch (NullPointerException e) {
            }
            try {
                born = doc.select("strong:contains(Born:) + a").first().text();
            } catch (NullPointerException e) {
            }
            try {
                bplace = doc.select("strong:contains(Birthplace/Hometown:) + a").first().text();
            } catch (NullPointerException e) {
            }
            try {
                nationality = doc.select("strong:contains(Nationality:) + a").first().text();
            } catch (NullPointerException e) {
            }
            try {
                agent = doc.select("strong:contains(Agent:) + a").first().text();
            } catch (NullPointerException e) {
            }
            try {
                drafted = doc.select("strong:contains(Drafted:) + a").first().text();
            } catch (NullPointerException e) {
            }
            try {
                hschool = doc.select("strong:contains(High School:) + a").first().text();
            } catch (NullPointerException e) {
            }

            // LinkedList<String> list = new LinkedList<String>();
            // Elements paragrahps = doc.select("p strong");
            // for (Element par : paragrahps) {
            // if (par.select())
            // i++;
            // if (i == 6 || i==7 || i==9)
            // continue;
            // else if (i > 10)
            // break;
            // list.add(p.text());
            // System.out.println(p.text());
            // }
            count++;
            if (count == 100)
                System.out.println(count);
            // System.out.println(nrm[0]);
            String line[] = { nrm[0], nrm[1], nrm[2], team, height, weight, born, bplace, nationality, agent, drafted,
                    hschool };
            finale.add(line);
            // StringTokenizer tokenizer = new StringTokenizer(weight,"Height: ");
            // while (tokenizer.hasMoreTokens())
            // System.out.println(tokenizer.nextToken());
        }
        writer.writeAll(finale);
        writer.flush();

        Document doc1 = Jsoup
                .connect("https://en.wikipedia.org/wiki/List_of_Women%27s_National_Basketball_Association_players")
                .get();
        Elements elements1 = doc1.select("div.div-col").select("li > a:not(.new)");

        for (Element link : elements1) {
            String linkHref = link.attr("abs:href");
            // System.out.println(linkHref);
            doc1 = Jsoup.connect(linkHref).get();
            String name = "", number = "", position = "", height = "", weight = "", team = "", born = "", bplace = "",
                    nationality = "", agent = "", drafted = "", hschool = "";
            try {
                name = doc1.select("h1[id=firstHeading]").first().text();
            } catch (NullPointerException e) {
            }
            try {
                number = doc1.select("th:matches(Number) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                position = doc1.select("th:matches(Position) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                weight = doc1.select("th:matches(Listed weight) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                height = doc1.select("th:matches(Listed height) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                team = doc1.select("th:matches(present) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                born = doc1.select("span[class=bday]").first().text();
            } catch (NullPointerException e) {
            }
            try {
                bplace = doc1.select("th:matches(Born) + td > a").first().text();
            } catch (NullPointerException e) {
            }
            try {
                nationality = doc1.select("th:matches(Nationality) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                agent = doc1.select("th:matches(Agent) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                drafted = doc1.select("th:matches(WNBA draft) + td").first().text();
            } catch (NullPointerException e) {
            }
            try {
                hschool = doc1.select("th:matches(High school) + td").first().text();
            } catch (NullPointerException e) {
            }

            count++;
            if (count == 100)
            System.out.println(count);
            String line[] = { name, number, position, team, height, weight, born, bplace, nationality, agent, drafted,
                    hschool };
            finale.add(line);
        }

        writer.writeAll(finale);
        writer.flush();

        // System.out.println(doc1.select("a").first().text());
        // System.out.println(doc1.select("div.stat-cells__cell").select("span").text());
        // break;
        // Elements paragrahps = doc1.select("p strong + a");
        // for (Element p : paragrahps) {
        // System.out.println(p.text());
        // i++;
        // if (i == 10)
        // break;
        // }
        count++;
        if (count == 100)
        System.out.println(count);
        // break;

        // File input2 = new File("/home/soros/homeworks/IDD -
        // homework2/html/euro1.html");
        // Document doc2 = Jsoup.parse(input2, "UTF-8",
        // "https://www.eurobasket.com/Euroleague/basketball-Players.aspx");
        // Elements elements2 =
        // doc2.select("td.fixedColLine");//select("td.fixedColLine");
        // int count = 0;
        // for (Element link : elements2) {
        // String linkHref = link.select("a").first().attr("abs:href");
        // // System.out.println(linkHref);
        // doc2 = Jsoup.connect(linkHref).get();
        // int i = 0;
        // Elements paragrahps = doc2.select("h1.player-title");
        // for (Element p : paragrahps){
        // System.out.println(p.text());
        // i++;
        // if (i==10) break;
        // }
        // count++;
        // }
        // System.out.println(count);

    }

}
