package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class AziendeFilippo {

    private static String stringify(List<String> list){
        String res = "";
        String last = list.remove(0);
        for (String string : list) {
            res += string + ", ";
        }
        return res + last;
    }

    public static void main(String[] args) throws IOException {

        List<String[]> finale = new ArrayList<String[]>();
        String[] intestazione = {"Name","World Rank (Sep-01-2021)","Annual Revenue in USD","Annual Net Income in USD","Market Value (Jan-01-2021)","Headquarters Country","Company Business", "Number of Employees", "Stock Category", "CEO", "Stock Exchange", "Company Website"};
        finale.add(intestazione);
        CSVWriter writer = new CSVWriter(new FileWriter("/home/zoso/Documents/IDD/homeworks/IDD - homework2/aziendefilippo.csv", true));

        for(int i=800; i<1000; i++){
            Document doc = Jsoup.connect("https://www.value.today/?title=&amp%3Bfield_company_category_primary_target_id=&amp%3Bfield_headquarters_of_company_target_id=&amp%3Bfield_company_website_uri=&amp%3Bfield_market_cap_aug_01_2021__value=&amp%3Bpage=7&field_company_category_primary_target_id&field_headquarters_of_company_target_id&field_company_website_uri=&field_market_cap_aug_01_2021__value=&page="+String.valueOf(i)).get();
            int count = 0;
            Elements companies, name, wrank, revenue, income, marketvalue, headquarter, business, employees, stockcategory, ceo, stockexchange, website;
            
            
            companies = doc.select("li[class=row well clearfix]");
            name = doc.select("h2.text-primary > a");
            // wrank = doc.select("div.field--name-field-world-rank-sep-01-2021- > div + div");
            // revenue = doc.select("div.field--name-field-revenue-in-usd > div + div");
            // income = doc.select("div.field--name-field-net-income-in-usd > div + div");
            // headquarter = doc.select("div.field--name-field-headquarters-of-company > div > div > a");
            // business = doc.select("div.field--name-field-company-category-primary > div + div");
            // employees = doc.select("div.field--name-field-employee-count > div + div");
            // stockcategory = doc.select("div.field--name-field-stock-category-lc > div + div");
            // ceo = doc.select("div.field--name-field-ceo > div + div");
            // stockexchange = doc.select("div.field--name-field-stock-exchange-lc > div + div");
            // website = doc.select("div.field--name-field-company-website > div + div > a");
                
            for (int j=0; j<10; j++){

                org.jsoup.nodes.Element company = companies.get(j);
                String nameS="", wrankS="", revenueS="", incomeS="", marketvalueS="", headquarterS="", businessS="", employeesS="", stockcategoryS="", ceoS="", stockexchangeS="", websiteS="";

                nameS = name.get(j).text();
                
                wrank = company.select("div.field--name-field-world-rank-sep-01-2021- > div + div");
                if (wrank.size() > 0)
                    wrankS =  wrank.first().text();

                revenue = company.select("div.field--name-field-revenue-in-usd > div + div");
                if (revenue.size() > 0)
                    revenueS =  revenue.first().text();
                
                income = company.select("div.field--name-field-net-income-in-usd > div + div");
                if (income.size() > 0)
                    incomeS = income.first().text();
                
                marketvalue = company.select("div.field--name-field-market-value-jan012021 > div + div");
                if (marketvalue.size() > 0)
                    marketvalueS = marketvalue.first().text();
                
                headquarter = company.select("div.field--name-field-headquarters-of-company > div > div > a");
                if (headquarter.size() > 0)
                    headquarterS = headquarter.first().text();
                
                business = company.select("div.field--name-field-company-category-primary > div + div");
                if (business.size() > 0)
                    businessS = stringify(business.first().children().eachText());
                
                employees = company.select("div.field--name-field-employee-count > div + div");
                if (employees.size() > 0)
                    employeesS = employees.first().text();

                stockcategory = company.select("div.field--name-field-stock-category-lc > div + div");
                if (stockcategory.size() > 0)
                    stockcategoryS = stringify(stockcategory.first().children().eachText());
                
                ceo = company.select("div.field--name-field-ceo > div + div");
                if (ceo.size() > 0)
                    ceoS =  stringify(ceo.first().children().eachText());
                
                stockexchange = company.select("div.field--name-field-stock-exchange-lc > div + div");
                if (stockexchange.size() > 0)
                    stockexchangeS = stringify(stockexchange.first().children().eachText());
                
                website = doc.select("div.field--name-field-company-website > div + div > a");
                if(website.size() > 0)
                    websiteS = website.first().attr("href");
                    
                
                // String line[]= {name.get(j).text(),
                //                 wrank.get(j).text(),
                //                 revenue.get(j).text(),
                //                 income.get(j).text(),
                //                 headquarter.get(j).text(),
                //                 stringify(business.get(j).children().eachText()),
                //                 employees.get(j).text(),
                //                 stringify(stockcategory.get(j).children().eachText()),
                //                 stringify(ceo.get(j).children().eachText()),
                //                 stringify(stockexchange.get(j).children().eachText()),
                //                 website.get(j).attr("href")
                //             };

                String line[] = {nameS, wrankS, revenueS, incomeS, marketvalueS, headquarterS, businessS, employeesS, stockcategoryS, ceoS, stockexchangeS, websiteS};
                
                // for (String s:line)
                //     System.out.println(s);
                // System.out.println();
                count++;
                if (count % 100 == 0) System.out.println(count);
                finale.add(line);
                
            }
            
        }
       
        writer.writeAll(finale);
        writer.flush();

    }


        
}
