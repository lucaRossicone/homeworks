package main;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * nome
 * industry
 * indirizzo
 * sito web
 * stock symbol
 */
public class MainAziende {

    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.connect("https://www.uspages.com/fortune500.htm").get();
        Elements elements = doc.select("div[id=main]").select("li > a");
        // int count = 0;
        for (Element link : elements) {
            String linkHref = link.attr("abs:href");
            // System.out.println(linkHref);
            doc = Jsoup.connect(linkHref).get();
            List<String> info = doc.select("div[id=aside]").select("ul[class=glance]").select("li > ul").select("li").eachText();
            List<String> info2 = doc.select("strong:matches(Contact Details) + ul").select("li").eachText();
            System.out.println("\n");
            info.forEach(k -> System.out.println(k));
            info2.forEach(k -> System.out.println(k));
        }
    }
}
