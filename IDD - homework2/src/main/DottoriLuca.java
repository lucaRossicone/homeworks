package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * nome
 * industry
 * indirizzo
 * sito web
 * stock symbol
 */
public class DottoriLuca {
    public static void main(String[] args) throws IOException {
        Document doc;
        try {
            doc = Jsoup.connect("https://health.usnews.com/doctors/specialists-index").get(); // eh m'hanno bannato
        } catch (Exception e) {
            System.out.println("T'hanno cacato subito");
            return;
        }

        Elements elements = doc.select("div[class=Content-sc-837ada-0 knwMaV content]").select("div.mb5")
                .select("li > a");
        int count = 0;

        List<String[]> finale = new ArrayList<String[]>();
        String[] intestazione = { "Name", "Specialization", "Subspecializations", "City", "Hospital", "Gender",
                "Years of Experience", "Languages", "Address", "Tel." };
        finale.add(intestazione);
        CSVWriter writer = new CSVWriter(new FileWriter("/home/zoso/Documents/IDD/homeworks/IDD - homework2/dottori.csv", true));

        String name = "", specialization = "", subspecialization = "", city = "", hospital = "", gender = "", yoe = "",
                languages = "", address = "", tel = "";

        for (Element link : elements) {
            String linkHref = link.attr("abs:href");
            // System.out.println(linkHref);
            try {
                doc = Jsoup.connect(linkHref).get();
            } catch (Exception e) {
                System.out.println("T'hanno cacato alla " + count + " pagina");
                break;
            }
            Elements el = doc.select("div[class=DetailCardDoctor__TitleWrapper-dno04z-8 iXGgV] > a");
            String linkHref2 = new String();
            for (Element lk : el) {
                System.out.println(count);
                String check = linkHref2;
                linkHref2 = lk.attr("abs:href");
                if (check.equals(linkHref2))
                    continue;
                try {
                    doc = Jsoup.connect(linkHref2).get();
                } catch (Exception e) {
                    System.out.println("T'hanno cacato alla " + count + " pagina");
                    break;
                }
                try {
                    name = (doc.select(
                            "h1[class=Heading__HeadingStyled-sc-1w5xk2o-0 gOHMnH Heading-sc-1w5xk2o-1 Hero__Name-sc-1lw4wit-3 kQuiLM fNlBNP]")
                            .text());
                } catch (Exception e) {
                }
                try {
                    specialization = (doc.select("p[class=Paragraph-sc-1iyax29-0 dikqyg]").text());
                } catch (Exception e) {
                }
                try {
                    subspecialization = (doc.select("p[class=Paragraph-sc-1iyax29-0 dVHoYt]").first().text());
                } catch (Exception e) {
                }
                try {
                    city = (doc.select("p[class=Paragraph-sc-1iyax29-0 Hero__Location-sc-1lw4wit-6 dikqyg hmSFHD]")
                            .text());
                } catch (Exception e) {
                }
                try {
                    hospital = (doc.select(
                            "p[class=Paragraph-sc-1iyax29-0 Hero__Location-sc-1lw4wit-6 dikqyg hmSFHD] + p[class=Paragraph-sc-1iyax29-0 dVHoYt]")
                            .text());
                } catch (Exception e) {
                }
                try {
                    gender = (doc.select("p[class=Paragraph-sc-1iyax29-0 gfICVI flex]").first().text());
                } catch (Exception e) {
                }
                try {
                    yoe = (doc.select("p[class=Paragraph-sc-1iyax29-0 gfICVI flex]:contains(Experience)").first()
                            .text());
                } catch (Exception e) {
                }
                try {
                    languages = (doc.select("p[class=Paragraph-sc-1iyax29-0 gfICVI]").first().text());

                } catch (Exception e) {
                }
                try {
                    address = (doc.select("p[class=Paragraph-sc-1iyax29-0 jdbslW]").text());

                } catch (Exception e) {
                }
                try {
                    tel = (doc.select("a[class=Anchor-byh49a-0 fPWGrk]").text());

                } catch (Exception e) {
                }
                System.out.print("\033[H\033[2J");
                System.out.flush();
                count++;
                if (count%100==0) {
                    writer.writeAll(finale);
                    writer.flush();
                    finale = new ArrayList<String[]>();
                }
                String line[] = { name, specialization, subspecialization, city, hospital, gender, yoe, languages,
                        address, tel };
                finale.add(line);
            }
        }
        writer.writeAll(finale);
        writer.flush();
        System.out.println(count);
    }
}
