package main;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.opencsv.CSVWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AziendeFroci {

    public static String[] mettilaNormaleDaiNonSeSaMai(String str) {
        String ruolo = new String();
        String numero = new String();
        StringBuilder nome = new StringBuilder();

        List<String> items = Arrays.asList(str.split("\\s* \\s*"));

        Collections.reverse(items);
        for (int i = 0; i < items.size(); i++) {
            ruolo = items.get(1);
            numero = items.get(0);
            if (i > 1)
                nome.append(items.get(i) + " ");
        }
        String[] res = { nome.toString(), ruolo, numero };
        return res;
    }

    public static void main(String[] args) throws IOException {

        List<String[]> finale = new ArrayList<String[]>();
        //String[] intestazione = {"Name","Address","Industry","Type","Est. of Ownership","National ID","SIC Code"};
        //finale.add(intestazione);
        CSVWriter writer = new CSVWriter(new FileWriter("/home/zoso/Documents/IDD/homeworks/IDD - homework2/aziende.csv", true));
        
        String azienda = "https://www.hithorizons.com/eu/analyses/country-statistics/moldova";
        Document doc = Jsoup.connect(azienda).get();
        Elements companies = doc.select("td.name > a[title]");
        for (Element companyURL : companies) {
            String company = companyURL.attr("abs:href");
            // System.out.println(linkHref);}
            // break;
            // System.out.println(company);
            Document doc1 = Jsoup.connect(company).get();
            String name = "", address = "", industry = "", type = "", est = "", nationalid = "", siccode = "";

            try {
                name = doc1.select("strong:matches(Name) + span").first().text();
            } catch (NullPointerException e) {
            }
            try {
                address = doc1.select("strong:matches(Address) + span").first().text();
            } catch (NullPointerException e) {
            }
            try {
                industry = doc1.select("strong:matches(Industry) + span").first().text();
            } catch (NullPointerException e) {
            }
            try {
                type = doc1.select("strong:matches(Type) + span").first().text();
            } catch (NullPointerException e) {
            }
            try {
                est = doc1.select("strong:matches(Est. of Ownership) + span").first().text();
            } catch (NullPointerException e) {
            }
            try {
                siccode = doc1.select("strong:matches(SIC Code) + span").first().text();
            } catch (NullPointerException e) {
            }
            try {
                nationalid = doc1.select("strong:matches(National ID) + span").first().text();
            } catch (NullPointerException e) {
            }

            // System.out.println(count);
            // System.out.println(nrm[0]);
            String line[] = { name, address, industry, type, est, nationalid, siccode };
            finale.add(line);
            // StringTokenizer tokenizer = new StringTokenizer(weight,"Height: ");
            // while (tokenizer.hasMoreTokens())
            // System.out.println(tokenizer.nextToken());
        }
        writer.writeAll(finale);
        writer.flush();

    }

}
