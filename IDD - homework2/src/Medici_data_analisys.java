import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.opencsv.CSVReader;

public class Medici_data_analisys {

    public static void trovaDoppi(Reader reader, Reader reader_) throws Exception {

            List<String[]> list = new ArrayList<>();
            CSVReader csvReader = new CSVReader(reader);
            String[] line;
            csvReader.readNext();
            while ((line = csvReader.readNext()) != null) {
                String name = line[0];
                line = name.split(" ");
                list.add(line);
            }
            reader.close();
            csvReader.close();

            System.out.println("Creata la prima lista...");

            CSVReader csvReader_ = new CSVReader(reader_);
            csvReader_.readNext();
            while ((line = csvReader_.readNext()) != null) {
                String name = line[0];
                String firstname;
                line = name.split(" ");
                for (String[] array : list) {
                    if (array[0].equals("Dr.")) firstname = array[1];
                    else firstname = array[0];
                    if (line[0].equals(firstname)){
                        for (String string : line) {
                            if (string.length() > 2 && !string.equals(line[0])) {
                                for (String cognome : array) {
                                    if (string.equals(cognome)){
                                        for (String s : array) System.out.print(s + " ");
                                        System.out.print("|| ");
                                        for (String s : line) System.out.print(s + " ");
                                        System.out.println();
                                    } 
                                }
                            }
                        }
                    }
                }
            }
            reader_.close();
            csvReader_.close();
    }

    public static void trovaRipetuti(Reader reader) throws Exception {

        CSVReader csvReader = new CSVReader(reader);
        String[] line;
        HashMap<String, Integer> map = new HashMap<>();
        csvReader.readNext();
        while ((line = csvReader.readNext()) != null) {
            String name = line[0];
            if (map.containsKey(name))
                map.put(name, map.get(name) + 1);
            else
                map.put(name, 1);
        }
        reader.close();
        csvReader.close();

        HashMap<Integer, Integer> map_ = new HashMap<>();
        map.forEach((k, v) -> {
            if (map_.containsKey(v))
                map_.put(v, map_.get(v) + 1);
            else
                map_.put(v, 1);
        });

        System.out.println("\n===== Distribuzione ripetizioni =====");
        map_.forEach((k, v) -> System.out.println("Un numero di entità pari a " + v + " è ripetuto esattamente " + k + " volte."));
        System.out.println("\n===== Numero entità distinte =====");
        System.out.println(map.size());
        System.out.println("(Dr. Jeffrey Abass è ripetuto 46 volte)");

    }

    public static void trovaNulli(Reader reader) throws Exception {
        CSVReader csvReader = new CSVReader(reader);
        String[] line;
        String[] intestazione = csvReader.readNext();
        int[] array = new int[intestazione.length];
        for (int i = 0; i < array.length; i++)
            array[i] = 0;

        while ((line = csvReader.readNext()) != null) {
            for (int i = 0; i < array.length; i++) 
                if (line[i].equals("")) array[i]++;
        }
        reader.close();
        csvReader.close();

        System.out.println("\n===== Numero di valori nulli per colonna =====");
        for (int i = 0; i < array.length; i++) {
            System.out.println("La colonna " + intestazione[i] + " ha un numero di valori nulli pari a " + array[i] + ".");
        }
    }


    public static void main(String[] args) throws Exception {

        trovaNulli(new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/aziendefilippo.csv"));
        //trovaRipetuti(new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/dottori.csv"));
        //trovaDoppi(new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/dottori.csv"),new FileReader("/home/zoso/Documents/IDD/homeworks/IDD - homework2/medici.csv"));
    }


}


